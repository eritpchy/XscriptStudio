--------------------------------------------------
-- Script name: Fast toast
-- Script version: 2.0
-- Script author: Jason
-- Script desc: Fast toast
--------------------------------------------------

function print(...)
	toast(json.encode{...}, 1)
	logcatWithLevel(2, ...)
	logfileWithLevel(2, ...)
end

-- main entry
function main()
	for i=1,1000000 do
		print(">>> " .. i .. " <<<")
	end
end

-- Call main entry, keep it under
main()