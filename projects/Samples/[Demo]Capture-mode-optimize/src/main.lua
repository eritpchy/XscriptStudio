--------------------------------------------------
-- Script name: Capture mode - optimize
-- Script version: 2.0
-- Script author: Jason
-- Script desc: A demo for screencap test
--------------------------------------------------

local timediff

function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

function reCapture()
    rotc = rotc + 1
    if rotc > #rot then rotc = 1 end
    xscript.setProfile("rotation_fix", rot[rotc])
    screencap(pic_path)
    ui.setImageView("iv", pic_path)
end

function scp()

    time = socket.gettime()
    -- save to disk , support format: bmp/png
    screencap(pic_path)
    timediff = socket.gettime() - time
    ui.newRow("Row1")
    ui.setTextColor("tv_maina", 0xFFFF0000)
    ui.addTextView("tv_mainb", string.format("   Save to disk time: %dms", timediff * 1000), 15, ui.MATCH_PARENT, ui.WRAP_CONTENT)
    ui.newRow("Row3")
    ui.addTextView("tv_mainc", "  Image path:" .. pic_path, 15, ui.MATCH_PARENT, ui.WRAP_CONTENT)
    ui.newRow("Row4")
    ui.addTextView("tv_maind", "   Fix roatation:", 15, ui.WRAP_CONTENT, ui.WRAP_CONTENT)
    ui.setTextColor("tv_maind", 0xFFFF0000)
    ui.newRow("Row5")
    ui.addTextView("tv_maine", "    [SAME AS YOU SEE]", 15, ui.WRAP_CONTENT, ui.MATCH_PARENT)
    ui.setTextColor("tv_maine", 0xFFFF0000)

    ui.addButton("xiuzheng", "GO →")
    ui.setOnClick("xiuzheng", "reCapture()")
    ui.newRow("Row6")
    ui.addImageView("iv", pic_path)
end

--
---------------------------------------------------------------------------------
-- new ui
function XUI()
    ui.newLayout("maindialog")
    ui.addTextView("tv_main", "  Capture mode - optimize", 15, ui.MATCH_PARENT, ui.WRAP_CONTENT)
    ui.newRow("Row0")
    ui.addTextView("tv_maina", "  Optimization mode supported, if you can see the screenshot", 15, ui.MATCH_PARENT, ui.WRAP_CONTENT)
    setTrigger.onUiShow("maindialog", "scp()")
    ui.setTitleText("maindialog", "Screencap optimization")
    ui.show("maindialog")
end

-- main entry
function main()
    xscript.setCompatible(false)

    scriptDir = xscript.scriptDir()

    --pic_path = scriptDir .. "/.screencap_op.png"
    pic_path = scriptDir .. "/.screencap_op.bmp"

    rot = { 0, 90, 180, 270 }
    rotc = 1;
    XUI()
end

-- Call main entry, keep it under
main()