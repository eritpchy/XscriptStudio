--------------------------------------------------
-- Script name: Keyboard function
-- Script version: 2.0
-- Script author: Jason
-- Script desc: A demo for keyboard function
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

-- main entry
function main()
    sleep(2000)
    for ii = 97, 97 + 25 do
        print("Press " .. string.char(ii), 490)
        sleep(500)
        key.click(string.char(ii))
    end

    print("Press Key: LEFT")
    sleep(2000)
    key.click("DPAD_LEFT")

    print("Press Key: DEL")
    sleep(2000)
    key.del()

    print("Press Key: BACKSPACE")
    sleep(2000)
    key.backSpace()

    print("Press Key: SPACE")
    sleep(2000)
    key.space()

    print("Press Key: ENTER")
    sleep(2000)
    key.enter()
    --[[
    print("Press Key: UNDO")
    sleep(2000)
    key.undo()
    --]]
    print("Press Key: TAB")
    sleep(2000)
    key.tab()

    print("Press Key: HOME")
    sleep(1000)
    key.click("HOME")
    print("Press Key: BACK")
    sleep(1000)
    key.click("BACK")
    print("Press Key: MENU")
    sleep(1000)
    key.click("MENU")
    print("Long press Key: MENU")
    sleep(1000)
    key.click("MENU", 1000)
    print("Long press Key: POWER")
    sleep(1000)
    key.down("POWER")
    sleep(2000)
    key.up("POWER")
    sleep(2000)
    key.power()
    sleep(2000)
    print("Press Key: VOLUME DOWN")
    key.volDown()
    sleep(2000)
    print("Press Key: VOLUME UP")
    key.volUp()
    sleep(2000)

    print("Press Key: HOME")
    sleep(2000)
    key.home()

    print("Press Key: CALL")
    sleep(2000)
    key.call()

    print("Press Key: END CALL")
    sleep(2000)
    key.endCall()

    print("Press Key: END BACK")
    sleep(2000)
    key.tab()

    print("按BACK 2")
    sleep(2000)
    key.back()

    print("Press Key: MENU")
    sleep(2000)
    key.menu()

    print("Press Key: END POWER")
    sleep(2500)
    key.power()
    sleep(2000)

    key.power()

    print("Press Key: END POWER")
    sleep(2000)
    print("Test complete!")
    sleep(2000)
end

-- Call main entry, keep it under
main()