--------------------------------------------------
-- Script name: Touch function
-- Script version: 2.0
-- Script author: Jason
-- Script desc: A demo for control touch.
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

-- main entry
function main()
    x, y = touch.scan("Please touch screen once")
    print("Coordinate:", x, y)


    touch.swipe(0, 0, 500, 600, 2000)
    sleep(1000)
    touch.down(200, 300, 1)
    touch.down(100, 100, 2)
    touch.down(500, 100, 3)
    touch.down(500, 100, 4)
    touch.down(500, 100, 5)
    touch.down(500, 100, 6)
    touch.down(500, 100, 7)
    touch.down(500, 100, 8)
    touch.down(500, 100, 9)
    touch.down(500, 100, 10)

    for iiii = 30, 130 do
        touch.move(iiii * 3, iiii * 2.5, 2)
        touch.move(iiii * 1.5, iiii * 1.5, 3)
        touch.move(iiii * 2.5, iiii * 1.5, 4)
        touch.move(iiii * 1.5, iiii * 3.5, 5)
        touch.move(iiii * 4.5, iiii * 1.5, 6)
        touch.move(iiii * 1.5, iiii * 5.5, 7)
        touch.move(iiii * 6.5, iiii * 1.5, 8)
        touch.move(iiii * 1.5, iiii * 7.5, 9)
        touch.move(iiii * 8.5, iiii * 1.5, 10)
        touch.move(iiii * 1.5, iiii * 9.5, 1)
        sleep(50)
    end
    touch.up(1)
    touch.up(2)
    touch.up(3)
    touch.up(4)
    touch.up(5)
    touch.up(6)
    touch.up(7)
    touch.up(8)
    touch.up(9)
    touch.up(10)

    print("Move to 400x500")
    sleep(1500)
    touch.down(100, 200)
    sleep(1000)
    touch.move(400, 500)
    sleep(1000)
    touch.up()
    sleep(1500)

    sleep(1500)

    print("Find icon and click")
    sleep(1500)

    isfound, x, y = find.color(0x97ca4b, 96)
    if isfound then
        print(string.format("click %s,%s", x, y))
        sleep(1500)
        touch.click(x, y)
    else
        print("Not found!")
    end


    sleep(1500)
    print("Test complete!")
end

-- Call main entry, keep it under
main() sleep(2000)