--------------------------------------------------
-- Script name: Capture mode - compatible
-- Script version: 2.0
-- Script author: Jason
-- Script desc: A demo for screencap test
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

function reCapture()
    rotc = rotc + 1
    if rotc > #rot then rotc = 1 end
    xscript.setProfile("rotation_fix", rot[rotc])
    screencap(pic_path)
    ui.setImageView("iv", pic_path)
end

function scp()
    local time, timediff
    time = os.clock()
    screencap()
    timediff = os.clock() - time

    -- save to disk , support format: bmp/png
    screencap(pic_path)

    ui.newRow("")
    ui.setTextColor("tv_maina", 0xFFFF0000)
    ui.addTextView("tv_mainb", string.format("   Capture time: %dms", timediff * 1000), 12)
    ui.newRow("")
    ui.addTextView("tv_mainc", "   Image path:" .. pic_path, 12)
    ui.newRow("")
    ui.addTextView("tv_maind", "    Fix roatation:", 12)
    ui.setTextColor("tv_maind", 0xFFFF0000)
    ui.newRow("")
    ui.addButton("xiuzheng", "Go →")
    ui.setOnClick("xiuzheng", "reCapture()")
    ui.addTextView("tv_maine", "    [SAME AS YOU SEE]", 10)
    ui.setTextColor("tv_maine", 0xFFFF0000)
    ui.newRow("")
    ui.addImageView("iv", pic_path)
end

--
---------------------------------------------------------------------------------
-- new ui
function XUI()
    ui.newLayout("maindialog")
    ui.addTextView("tv_maina", "  Compatible mode supported, if you can see the screenshot", 12)
    setTrigger.onUiShow("maindialog", "scp()")
    ui.setTitleText("maindialog", "Screencap compatible")
    ui.show("maindialog")
end

-- main entry
function main()
    toast("in...")
    xscript.setCompatible(true)

    scriptDir = xscript.scriptDir()

    pic_path = scriptDir .. "/.screencap_co.png"
    --pic_path = scriptDir .. "/.screencap_co.bmp"

    rot = { 0, 90, 180, 270 }
    rotc = 1;
    XUI()
end

-- Call main entry, keep it under
main()