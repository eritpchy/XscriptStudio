--------------------------------------------------
-- Script name: Asyc caller function
-- Script version: 2.0
-- Script author: Jason
-- Script desc: A demo for async callee
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

function ontimeloop()
    if gVar.ii > 10 then
        exit()
    end

    print("Loop , count: " .. gVar.ii .. "passing time:" .. (socket.gettime() - gVar.timer) * 1000 .. "ms")
    gVar.ii = gVar.ii + 1
end

function ontime()
    print("Trigger!, passing time:" .. (socket.gettime() - gVar.timer) * 1000 .. "ms")
end

-- main entry
function main()
    print("Delay 3 sec to call function ontime")
    gVar = thread.newGloablVar("gVar")
    gVar.timer = socket.gettime()
    gVar.ii = 1

    setTrigger.ontime(3000, ontime)
    setTrigger.timeloop(2000, ontimeloop)

    -- keep main thread alive
    for ii = 1, 100000 do
        sleep(200)
    end
end

-- Call main entry, keep it under
main()