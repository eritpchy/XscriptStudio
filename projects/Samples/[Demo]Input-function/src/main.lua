--------------------------------------------------
-- Script name: Input function
-- Script version: 2.0
-- Script author: Jason
-- Script desc: A demo for input function
--------------------------------------------------

-- main entry
function main()
    ime.inputText("Hello!")
end

-- Call main entry, keep it under
main()