--------------------------------------------------
-- Script name: Xscript Web
-- Script version: 2.0
-- Script author: Jason
-- Script desc: Web site
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

function XUI()
    ui.newLayout("main")
    ui.addWebView("web", "https://www.xdow.net")
    ui.setTitleText("main", "Home")
    ui.show("main")
end

-- main entry
function main()
end

XUI()

-- Call main entry, keep it under
main()