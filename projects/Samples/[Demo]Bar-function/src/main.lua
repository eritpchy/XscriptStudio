--------------------------------------------------
-- Script name: Bar function
-- Script version: 2.0
-- Script author: Jason
-- Script desc: A demo for Xscript control bar.
--------------------------------------------------
function syncToast(msg)
    toast(msg, 2000)
    sleep(2000)
end

-- main entry
function main()
    syncToast("Bar function")
    sleep(1000)
    local x, y = bar.position()
    syncToast("Move bar to 200,200")
    bar.position(200, 200)
    sleep(2000)
    syncToast("Hide current bar")
    bar.hide()
    sleep(2000)
    syncToast("Is bar shown?: " .. tostring(bar.isShown()))
    sleep(2000)
    syncToast("Show current bar")
    bar.show()
    sleep(2000)
    syncToast("Is bar shown?: " .. tostring(bar.isShown()))
    bar.position(x, y)
    syncToast("Test complete!")
    sleep(1000)
end

-- Call main entry, keep it under
main()