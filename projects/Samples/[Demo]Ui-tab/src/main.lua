--------------------------------------------------
-- Script name: Ui Tab demo
-- Script version: 2.0
-- Script author: Jason
-- Script desc: A demo for tab ui
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

function XUI()
    -- Page 1
    ui.newLayout("1")
    ui.addTextView("tv1_1", "Page 1")
    ui.newRow("row1")
    ui.addTextView("tv1_2", "This is page 1")
    ui.newRow("row2")
    ui.addTextView("tv1_3", "This is page 1")

    -- Page 2
    ui.newLayout("2")
    ui.addTextView("tv2_1", "Page 2")
    ui.newRow("row3")
    ui.addEditText("et2_1", "Note", ui.MATCH_PARENT, ui.WRAP_CONTENT)

    -- Page 3
    ui.newLayout("3")
    ui.addTextView("tv3_1", "Page 3")
    ui.newRow("row4")
    ui.addRadioGroup("rg3_1", { '1\n', '2', '3\n', '4', '5', '6' }, 2)

    -- main layout
    ui.newLayout("main")
    -- create tab container, name:tab
    ui.addTabView("tab")
    -- put page 1 into tab container
    ui.addTab("page 1", "tab", "1")
    -- put page 2 into tab container
    ui.addTab("page 2", "tab", "2")
    -- put page 3 into tab container
    ui.addTab("page 3", "tab", "3")
    -- set layout title
    ui.setTitleText("main", "Tab Ui")
    -- show main layout
    ui.show("main")
end

-- main entry
function main()
end

XUI()

-- Call main entry, keep it under
main()