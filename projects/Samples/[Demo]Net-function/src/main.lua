--------------------------------------------------
-- Script name: Net function
-- Script version: 2.0
-- Script author: Jason
-- Script desc: A demo for control net function
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

-- Call back function
function httpCallBack(data, code)
    print("Content：", code, data)
end

-- main entry
function main()
    print(net.get("http://www.qicaispace.com/teach/asp/page04/info45.asp"))

    net.asyncGet("http://1212.ip138.com/ic.asp", httpCallBack, 10)

    sleep(3000)

    print("Net time:[" .. os.date("%c", net.time()) .. "]")
    sleep(2000)

    print("Download Google home page  ")
    local res, code = net.get("http://www.google.com")
    if res ~= nil then
        print("Http return:" .. code .. "\nContent:" .. res)
        sleep(2000)

        content = net.stripHtml(res)
        print("Strip html content:", content)
        sleep(2000)
    end

    print("Test complete!")
    sleep(2000)
end

-- Call main entry, keep it under
main()