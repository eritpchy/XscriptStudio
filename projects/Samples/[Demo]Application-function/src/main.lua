--------------------------------------------------
-- Script name: Application function
-- Script version: 2.0
-- Script author: Jason
-- Script desc: A demo for control app.
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

-- main entry
function main()
    print("Application function")
    sleep(1500)
    print(string.format("Front app package name:%s\nmy package name:%s", app.getfront(), app.getSelf()))
    sleep(1500)

    print("Check web browser is available on device.")
    sleep(1500)
    isinstalled = app.isinstall("com.android.browser")
    print("Web browser installed.")
    sleep(1500)

    print("Now, switch to web browser.")
    app.start("com.android.browser")
    sleep(1500)

    print(string.format("Front app package name:%s", app.getfront()))
    sleep(1500)

    print("Swich to last app")
    sleep(1500)
    app.switch()
    sleep(1500)

    print("Switch to web browser.")
    sleep(1500)
    app.switch("com.android.browser")
    sleep(1500)

    print("Kill web browser.")
    sleep(1500)
    app.stop("com.android.browser")


    print("Disable web browser.")
    sleep(1500)
    app.disable("com.android.browser")
    sleep(5000)

    print("Enable web browser.")
    sleep(1500)
    app.enable("com.android.browser")
    sleep(5000)

    --print("Clean web browser data.")
    --sleep(1500)
    --app.clear("com.android.browser")
    --sleep(3000)

    print("Install app: /sdcard/Download/test.apk")
    sleep(1500)
    is_success = app.install("/sdcard/Download/test.apk")


    print("Unistall app: com.xunlei.downloadprovider")
    sleep(1500)
    is_success = app.unInstall("com.xunlei.downloadprovider")

    print("Kill background app, free memory")
    sleep(1500)
    app.killall()
    sleep(1500)

    print("List all installed app.")
    sleep(1500)
    app_list = app.list()
    ii = 1
    ui.newLayout("main")
    while (app_list[ii] ~= nil) do
        ui.addTextView("app_" .. ii, app_list[ii])
        ui.newRow("")
        ui.addLine("line_" .. ii)
        ui.newRow("")
        ii = ii + 1
    end
    ui.setTitleText("main", "app")
    ui.show("main")
    print("Test complete!")
    sleep(1500)
end

-- Call main entry, keep it under
main()