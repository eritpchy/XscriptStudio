--------------------------------------------------
-- Script name: Xscript function
-- Script version: 2.0
-- Script author: Jason
-- Script desc: A demo for control xscript
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

-- main entry
function main()
    scriptDir = xscript.scriptDir()

    print(string.format("Script Path:%s", xscript.scriptPath()))
    sleep(2000)
    print(string.format("Script Dir:%s", scriptDir))
    sleep(2000)

    print(string.format("Capture mode - compatible? :%s", xscript.isCompatible()))
    sleep(2000)
    xscript.setCompatible(true)
    print(string.format("Force capture mode to compatible:%s", xscript.isCompatible()))
    sleep(2000)
    xscript.setCompatible(false)
    print(string.format("Force capture mode to optimize:%s", xscript.isCompatible()))
    sleep(2000)
    print("Set profile:isCompatible=1")
    sleep(2000)
    xscript.setProfile("isCompatible", "1")
    print("Script pause")
    sleep(2000)
    xscript.pause()
    print("Script resume")
    sleep(2000)
    print("Script stop!")
    sleep(2000)
    xscript.stop()
    print("YOU SHOULD NOT SEE THIS !!!")
    sleep(2000)
end

-- Call main entry, keep it under
main()