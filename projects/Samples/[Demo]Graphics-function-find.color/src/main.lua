--------------------------------------------------
-- Script name: Graphics function - find color
-- Script version: 2.0
-- Script author: Jason
-- Script desc: A demo for graphics function - find color
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

-- main entry
function main()
    print("Graphics function - find color")
    sleep(1500)
    -- Force capture screen to memory
    -- screencap()
    -- cap_and_find_path = "/sdcard/Android/XScript/Dev/find_test.png"
    -- screencap(cap_and_find_path,0,0,100,200)
    autoscreencap(false)
    sleep(5000)
    touch.click(100, 100)
    touch.down(0, 0)

    while true do
        screencap()
        sleep(200)
        timer = socket.gettime()

        flag, x, y = find.color(0x0000FF, 95)
        print(string.format("find.color blue, fuzzy:95 :x=%s ,y=%s, time:%dms", x, y, (socket.gettime() - timer) * 1000), flag)
        touch.down(0, 0)
        sleep(100)
        touch.up(x, y)
    end

    timer = socket.gettime()
    flag, x, y = find.color(0x6EAB19, 90)
    print(string.format("find.color 0x6EAB19, fuzzy:90:x=%s ,y=%s, time:%dms", x, y, (socket.gettime() - timer) * 1000))
    touch.down(x, y)
    sleep(2000)
    touch.up(x, y)


    timer = socket.gettime()
    flag, x, y = find.color(0x6EAB19, 90, 1, 100, 500, 500)
    print(string.format("find.color 0x6EAB19 :x=%s ,y=%s, time:%dms", x, y, (socket.gettime() - timer) * 1000))
    touch.down(x, y)
    sleep(2000)
    touch.up(x, y)
    sleep(2000)
    if flag then
        touch.click(x, y)
        sleep(2000)
        -- autoscreencap(false)
        timer = socket.gettime()
        flag, x, y = find.color(0x6EAB19, 80, x + 1, y + 1, x + 5, y + 5)
        print(string.format("find.color 0x6EAB19 x=%s y=%s, time:%dms", x, y, (socket.gettime() - timer) * 1000))
        touch.down(x, y)
        sleep(2000)
        touch.up(x, y)
    end

    xscript.stop()
end

-- Call main entry, keep it under
main()