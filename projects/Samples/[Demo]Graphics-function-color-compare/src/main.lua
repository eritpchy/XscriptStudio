--------------------------------------------------
-- Script name: Graphics function - color compare
-- Script version: 2.0
-- Script author: Jason
-- Script desc: A demo for graphics function - color compare
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

-- main entry
function main()
    touch.click(100, 100)
    print(getcolor(100, 100))
    autoscreencap(false)
    -- Force capture screen to memory
    screencap()
    timer = os.clock()
    for ii = 1, 100 do
        iscolor(58, 44, 0x97ca4b, 99)
    end

    print("Time:" .. os.clock() - timer)
    sleep(2000)
end

-- Call main entry, keep it under
main()
