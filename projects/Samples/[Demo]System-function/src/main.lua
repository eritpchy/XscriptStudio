--------------------------------------------------
-- Script name: System function
-- Script version: 2.0
-- Script author: Jason
-- Script desc: A demo for control system.
--------------------------------------------------

function print(...)
    toast(json.encode{...})
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

-- main entry
function main()
    print("Set brightness to max")
    system.setBrightness(100)
    sleep(3000)
    print("Set brightness to min")
    system.setBrightness(0)
    sleep(3000)
    print("set brightness to auto")
    system.setBrightness(-1)
    sleep(3000)
    print("Disable WIFI");
    system.setWifi(false)
    sleep(3000)
    print("Enable WIFI");
    system.setWifi(true)
    sleep(1000)
    print("Set clipboard");
    system.setClip("1234567890")
    sleep(1000)
    print("Read clipboard: [" .. system.getClip() .. "]")
    sleep(1000)
    print("Test complete!")
    sleep(2000)
end

-- Call main entry, keep it under
main()
