--------------------------------------------------
-- Script name: Json function
-- Script version: 2.0
-- Script author: Jason
-- Script desc: A demo for json data parse
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

-- main entry
function main()
    local _jsonArray = {}
    _jsonArray[1] = 8
    _jsonArray[2] = 9
    _jsonArray[3] = 11
    _jsonArray[4] = 14
    _jsonArray[5] = 25

    local _arrayFlagKey = {}
    _arrayFlagKey["array"] = _jsonArray

    local tab = {}
    tab["Himi"] = "Xscript"
    tab["testArray"] = _arrayFlagKey
    tab["age"] = "23"

    local jsonData = json.encode(tab)

    print(jsonData)
    sleep(2000)
    -- print： {"age":"23","testArray":{"array":[8,9,11,14,25]},"Himi":"Xscript"}

    local data = json.decode(jsonData)
    local a = data.age
    local b = data.testArray.array[2]
    local c = data.Himi

    print("a:" .. a .. "  b:" .. b .. "  c:" .. c)
    sleep(2000)
    -- print： a:23  b:9  c:Xscript
end

-- Call main entry, keep it under
main()