--------------------------------------------------
-- Script name: Control panel settings
-- Script version: 2.0
-- Script author: Jason
--------------------------------------------------
function printValues()
    while true do
        sleep(2000)
        local uiCache = ui.getData()
        toast(string.format("Wedget 1:%s\nWedget 2:%s\nWedget 3:%s\nWedget 4:%s\nWedget 5:%s\nWedget 6:%s",
            uiCache["ed1"], uiCache["sb1"], uiCache["rg1"], uiCache["rg2"], uiCache["rg3"], uiCache["rg4"], uiCache["ed1"]), 2000)
    end
end

-- main entry
function main()
    printValues()
end


-- Call main entry, keep it under
main()
