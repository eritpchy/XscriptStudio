monkeyThread = thread.newGlobalVar("monkeyThread")
function watchAppAlive(packageName)
	thread.start(function()
		while true do
			sleep(5000)
			bar.hide()
			local currentPackageName = app.getFront()
			if (packageName == currentPackageName) then
				monkeyThread.stop = false
				continue
			end
			monkeyThread.stop = true
			app.start(packageName)
			sleep(3000)
		end
	end)
end

function monkey()
	local screenWidth, screenHeight = systeminfo.displaySize()
	local y = systeminfo.statusBarHeight() + 10
	while true do
		if (monkeyThread.stop) then
			sleep(3000)
			continue
		end
		local randomX = math.random(0, screenWidth)
		local randomY = math.random(y, screenHeight)
		touch.click(randomX, randomY)
		sleep(50)
		local randomX = math.random(0, screenWidth)
		local randomY = math.random(y, screenHeight / 10)
		touch.click(randomX, randomY)
		sleep(50)
		logcat("running")
	end
end

function main()
	local packageName = app.getSelf()
	watchAppAlive(packageName)
	monkey()
end



main() --> 调用main函数, 请保持最后一行

