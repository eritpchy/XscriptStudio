--------------------------------------------------
-- Script name: System information function
-- Script version: 2.0
-- Script author: Jason
-- Script desc: A demo for system information
--------------------------------------------------

-- print to ui
count = 0
function print(...)
    local arg = { ... }
    for i, v in ipairs(arg) do
        v = tostring(v)
        count = count + 1
        ui.addTextView("main_ui_textview_N" .. count, v, 16, ui.MATCH_PARENT, ui.WRAP_CONTENT)
    end
    ui.newRow("main_ui_row_N" .. count)
end

function start()
    w, h = systeminfo.deviceSize()
    print(string.format("Device size:w=%s h=%s", w, h))
    w, h = systeminfo.displaySize()
    print(string.format("Screen size:w=%s h=%s", w, h))
    dpi = systeminfo.displayDpi()
    print(string.format("Screen dpi:%d", dpi))

    if systeminfo.networkstate() then
        print("Network connected.")
    else
        print("Network disconnect.")
    end

    if systeminfo.wifistate() then
        print("WIFI connected.")
    else
        print("WIFI disconnect.")
    end

    print("System bar height:" .. systeminfo.statusBarHeight())
    print("Screen rotation: " .. systeminfo.rotation())
    print("Screen orientation: " .. systeminfo.orientation())
    print("SD path: " .. systeminfo.sdpath())
    print(string.format("Bluetooth MAC:%s", systeminfo.bluetoothmac()))
    print(string.format("WIFI MAC:%s", systeminfo.wifimac()))
    print(string.format("UDID:%s", systeminfo.udid()))
    print(string.format("IMEI:%s", systeminfo.imei()))
    print(string.format("Model:%s", systeminfo.model()))
    print(string.format("sdk:%s", systeminfo.sdkversion()))
    print(string.format("Release:%s", systeminfo.releaseversion()))
    print(string.format("Board:%s", systeminfo.board()))
    print(string.format("BOOTLOADER:%s", systeminfo.bootloader()))
    print(string.format("BRAND:%s", systeminfo.brand()))
    print(string.format("CPU_ABI:%s", systeminfo.cpuabi()))
    print(string.format("CPU_ABI2:%s", systeminfo.cpuabi2()))
    print(string.format("DISPLAY:%s", systeminfo.display()))
    print(string.format("Finger print:%s", systeminfo.fingerprint()))
    print(string.format("Hardware:%s", systeminfo.hardware()))
    print(string.format("HOST:%s", systeminfo.host()))
    print(string.format("ID:%s", systeminfo.id()))
    print(string.format("Manufacturer:%s", systeminfo.manufacturer()))
    print(string.format("Product:%s", systeminfo.product()))
    print(string.format("Radio:%s", systeminfo.radio()))
    print(string.format("Serial:%s", systeminfo.serial()))
    print(string.format("Read sys prop: dhcp.wlan0.ipaddress: %s", systeminfo.getProp("dhcp.wlan0.ipaddress")))
    ui.refresh()
    sleep(1500)
end

function XUI()
    ------------- UI START---------------
    ui.newLayoutEmpty("main_ui")

    ui.addTextView("main_ui_textview_1", "\nSysteminfo\n", 18, ui.matchParent, ui.wrapContent)
    ui.newRow("main_ui_row_1")
    ui.addButton("main_ui_btn", "press to start")
    ui.setOnClick("main_ui_btn", "start() ")
    ui.newRow("main_ui_row_2")


    ui.setBackground("main_ui", 0xFFCCD8E9)

    ui.show("main_ui")
    ------------- UI END-----------------
end

XUI()
