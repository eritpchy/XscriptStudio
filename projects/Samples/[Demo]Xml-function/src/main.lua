--------------------------------------------------
-- Script name: Xml function
-- Script version: 2.0
-- Script author: Jason
-- Script desc: A demo for xml data parse
--------------------------------------------------


-- print to ui
function print(...)
    local arg = { ... }
    for i, v in ipairs(arg) do
        v = tostring(v)
        ui.addTextView("main_ui_textview_N", v, 16, ui.matchParent, ui.wrapContent)
        ui.addTextView("main_ui_textview_N", " ", 16, ui.matchParent, ui.wrapContent)
    end
    ui.newRow("main_ui_row_N")
end

function X_xml()
    ui.addTextView("main_ui_textview_2", "--------------", 16, ui.matchParent, ui.wrapContent)
    ui.newRow("main_ui_row_3")
    ui.addTextView("main_ui_textview_3", "result:", 16, ui.matchParent, ui.wrapContent)
    ui.newRow("main_ui_row_4")
    ui.addTextView("main_ui_textview_4", "from .xml.xml:", 16, ui.matchParent, ui.wrapContent)
    ui.newRow("main_ui_row_5")

    -- load XML data from file ".xml" into local table xfile
    local success, xfile = pcall(function() return xml.load(xscript.scriptDir() .. "/.xml.xml") end)
    if success == false or xfile == nil then success, xfile = pcall(function() return xml.load(xscript.scriptDir() .. "/xml.xml") end) end
    if success == false or xfile == nil then success, xfile = pcall(function() return xml.load("/sdcard/Android/XScript/示例脚本/.xml.xml") end) end
    if success == false or xfile == nil then success, xfile = pcall(function() return xml.load("/sdcard/Android/XScript/Demo/.xml.xml") end) end

    print(success, xfile)
    -- search for substatement having the tag "scene"
    local xscene = xfile:find("scene")
    -- if this substatement is found
    if xscene ~= nil then
        --  print it to screen
        print(xscene)
        --  print  tag, attribute id and first substatement

        print(xscene:tag())
        print(xscene.id)
        print(xscene[1])
    end
    --获取当前运行脚本所在文件夹
    local dir = xscript.scriptDir()
    print(dir)
    xfile:save(dir .. "/t.xml")
    print("---END---.")
end


function XUI()
    ------------- UI START---------------
    ui.newLayout("main_ui")
    ui.addTextView("main_ui_textview_1", "\nXml function\n", 18, ui.matchParent, ui.wrapContent)
    ui.newRow("main_ui_row_1")
    ui.addButton("main_ui_btn", "press to start")
    ui.setOnClick("main_ui_btn", "X_xml() ")
    ui.newRow("main_ui_row_2")

    ui.show("main_ui")
    ------------- UI END-----------------
end

-- main entry
function main()
end

XUI()

-- Call main entry, keep it under
main()