--------------------------------------------------
-- Script name: Local html ui
-- Script version: 2.0
-- Script author: Jason
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

function XUI()
    ui.newLayout("main")
    ui.addWebView("web", "file://" .. xscript.scriptDir() .. "/local.html")
    ui.setEventHandler("web", function(id, type, ...)
        local args = { ... }
        print("Click", id, type, unpack(args))
        print(args[1])
    end)
    ui.show("main")
end

-- main entry
function main()
end

XUI()

-- Call main entry, keep it under
main()
