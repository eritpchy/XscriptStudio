--------------------------------------------------
-- Script name: Ui demo
-- Script version: 2.0
-- Script author: Jason
-- Script desc: A demo for ui all
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

print("md5:" .. security.md5("aaaaa"))
print("base64:" .. security.base64enc("aaaaa"))
print("base64dec:" .. security.base64dec(security.base64enc("aaaaa")))

function scp()
    scriptDir = xscript.scriptDir()
    pic_path = scriptDir .. ".screencap_test.png"
    screencap(pic_path, 0, 0, 100, 200)
    ui.addImageView("iv", pic_path)
    ui.setGravity("row5", 17)
    ui.setOnClick("iv", "print(\"test:setOnClick:ImageView\")")
end

function onClick()
    print("Btn press")
    ui.updateResult()
    result = ui.getResult("et_main")
    isChecked = ui.getResult("checkbox_main")
    selected = ui.getResult("rg")
    spResult = ui.getResult('spinner')

    ui.newRow("row6")
    ui.addTextView("tv_third", string.format("text: %s type:%s", result, type(result)))

    ui.newRow("row7")
    ui.addTextView("tv_4", string.format("Checkbox select: %s  type:%s", isChecked, type(isChecked)))

    ui.newRow("row8")
    ui.addTextView("tv_5", string.format("Radiobox select: %s type:%s", selected, type(selected)))

    ui.newRow("row9")
    ui.addTextView("tv_6", string.format("Spinner select: %s type:%s", spResult, type(spResult)))
    print("Change onClick to setText")
    ui.setOnClick("btn", "btn_onclick()")
    --ui.setEnabled("spinner",false)
    ui.setTextView("tv_main", "Text +1!", 12, ui.WRAP_CONTENT, ui.WRAP_CONTENT)
    ui.setEditText("et_main", "hint  +1", ui.WRAP_CONTENT, ui.WRAP_CONTENT)
    ui.setRadioGroup("rg", { '4', '5', '6' }, 2)
    ui.setSpinner("spinner", { "1st  +1", "2nd  +1 ", "3   +1", "4   +1", "5   +1" }, 2, -1, -1)
    ui.setCheckBox("checkbox_main", "checkbox  +1", true)
    ui.setButton("btn", "btn text  +1")
    ui.setImageView("iv", xscript.scriptDir() .. ".dama.png")
end

ii = 0
function btn_onclick()
    ii = ii + 1
    ui.setText("btn", ii)
end

---------------------------------------------------------------------------------
ui.newLayout("maindialog")

---------------------------------------------------------------------------------
ui.newRow("row0")
ui.addTextView("tv_main", "Hello", 15, ui.MATCH_PARENT, ui.WRAP_CONTENT)

--ui.setBackground("tv_main",0xFF006699)

--ui.setTextColor("tv_main",0xFFFFFFFF)
ui.setPadding("tv_main", 10, 5, 10, 5)
ui.setGravity("tv_main", ui.CENTER)
---------------------------------------------------------------------------------
ui.newRow("row1")
ui.addEditText("et_main", "hint", ui.WRAP_CONTENT, ui.WRAP_CONTENT)

ui.addEditText("et_main1", "12345", ui.MATCH_PARENT, ui.WRAP_CONTENT)
ui.setText("et_main", "hello")
ui.setOnClick("et_main", "print(\"Edittext click\",200)")
---------------------------------------------------------------------------------
ui.newRow("row2")
ui.addRadioGroup("rg", { '1\n', '2', '3\n', '4', '5', '6' }, 2)
ui.setTextColor("rg", 0xFF3366, 1)
ui.setOnClick("rg", "print(\"RadioBox click\",200)")
---------------------------------------------------------------------------------
ui.newRow("row3")
ui.addSpinner("spinner", { "1st item", "2nd item", "3", "4", "5" }, 2, ui.WRAP_CONTENT, ui.MATCH_PARENT)
ui.newRow("row4")
ui.addLine("")
--ui.setPadding("spinner",30,0,0,0)
--ui.setPadding("row3",30,0,0,0)
ui.setTextColor("spinner", 0x33ff33, 0)
ui.setTextColor("spinner", 0xFF3366, 2)
ui.newRow("row4")
ui.setOnClick("spinner", "print(\"Spinner click\",200)")
---------------------------------------------------------------------------------
ui.newRow('2')
ui.addCheckBox("checkbox_main", "checkbox text", false)
ui.newRow('1')
ui.setOnClick("checkbox_main", "print(\"Checkbox click\",200)")
---------------------------------------------------------------------------------
ui.addButton("btn", "btn text")
ui.setGravity("btn", 17)
ui.addButton("btn2", "gone")
function gone()
    ui.setVisibility("btn2", ui.GONE)
end

ui.setOnClick("btn2", "gone()")
ui.addButton("btn3", "hide")
ui.setOnClick("btn3", "	ui.setVisibility(\"btn3\",ui.INVISIBLE)")
ui.addButton("btn4", "4")

ii = "0"
function btn5call(ii)
    ii = tonumber(ii)
    ii = ii + 1
    ii = tostring(ii)

    ui.newLayout("btn5_ui" .. ii)
    ui.addButton("btn5" .. ii, "Onclick loop" .. ii)
    ui.setOnClick("btn5" .. ii, " btn5call(" .. ii .. ")")
    ui.addCheckBox("checkbox_main_sub", "Checkbox", false)
    ui.setCheckBox("checkbox_main_sub", "Checkbox", true)

    ii = tostring(ii)
    ui.show("btn5_ui" .. ii)
end

ui.addButton("btn5", "Onclick loop")
--ui.setBackground("btn5",0xFFFF0000)
ui.setOnClick("btn5", "btn5call(" .. ii .. ")")

ui.newRow("row5")

---------------------------------------------------------------------------------


ui.loadProfile()
ui.updateResult()
ui.setOnClick("btn", "print(\"enable onClick() start\")  onClick() ")
--ui.setOnClick("btn", "ui.dismiss('maindialog') ")


ui.setTitleText("maindialog", "Ui demo")
ui.setTitleBackground("maindialog", 0xFFFF0000)
--ui.setBackground("maindialog",0xFF92b85c)
ui.setFullScreen("maindialog")
--ui.setBackground("maindialog",0x33ff33)
ui.setOnClick("tv_main", "print(\"Text onClick\",200)")
setTrigger.ontime(50, scp)
ui.show("maindialog")


ui.saveProfile()
--	button.setPadding(left, top, right, bottom);


