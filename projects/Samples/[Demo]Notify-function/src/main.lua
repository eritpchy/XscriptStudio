--------------------------------------------------
-- Script name: Notify function
-- Script version: 2.0
-- Script author: Jason
-- Script desc: A demo for notify function
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

-- main entry
function main()

    print("Vibrate 1 sec")
    sleep(1500)
    notify.vibrate(1000)

    print("Vibrate 5 sec")
    sleep(1500)
    notify.vibrate(5000)

    print("Toast 10 sec")
    sleep(1500)
    notify.toast("Hello World", 10000)
    sleep(10000)

    print("Play media")
    sleep(1500)
    notify.player("/system/media/audio/ringtones/Rush.ogg")
    sleep(2000)
    notify.player("STOP")

    print("Test complete!")
    sleep(1500)
end

-- Call main entry, keep it under
main()