--------------------------------------------------
-- Script name: Ime list
-- Script version: 2.0
-- Script author: Jason
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

function getIme()
    local list = {}
    local str
    local imeTmp = xscript.scriptDir() .. ".ime"
    -- Note! dont use io.popen, will forzen on some Android device
    os.execute("ime list -s > " .. imeTmp .. ";exit")
    str = xutil.readFileAll(imeTmp)
    if (str ~= nil) then
        list = xutil.split(str, "\n")
    end
    return list
end

function copy(id)
    system.setClip(id)
    print("Copyed [" .. id .. "]", 1000)
end

-- main entry
function main()
    local i
    local imes = getIme()
    ui.newLayout("main")
    ui.setTitleText("main", "List ime")
    logcat(#imes)
    ui.addTextView("label_copy", "Press to copy")
    ui.newRow("")
    for i = 1, #imes do
        if imes[i] and imes[i]:len() > 0 then
            local imeID = "ime_" .. i
            ui.addTextView(imeID, imes[i], 14, ui.WRAP_CONTENT, 40)
            ui.setGravity(imeID, ui.BOTTOM)
            ui.setOnClick(imeID, "copy([[" .. imes[i] .. "]])")
            ui.newRow("row1_" .. i)
            ui.addLine("line_" .. i)
            ui.newRow("row2_" .. i)
        end
    end
    ui.setTitleText("main", "Ime list")
    ui.show("main")
end

-- Call main entry, keep it under
main()
