--------------------------------------------------
-- Script name: Web browser
-- Script version: 2.0
-- Script author: Jason
-- Script desc: A small web browser
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

function GOTO()
    ui.updateResult()
    local url = ui.getResult("et")
    if (url ~= nil and type(url) == "string") then
        print(url)
        ui.setWebView("web", "http://" .. url)
        ui.refresh()
    end
end

function XUI()
    ui.newLayout("main")
    ui.addButton("btn", "Go")
    ui.addEditText("et", "baidu.com", ui.MATCH_PARENT, ui.WRAP_CONTENT)

    ui.setOnClick("btn", "GOTO()")
    ui.newRow("")
    ui.addWebView("web", "http://baidu.com")
    -- set main layout title
    ui.setTitleText("main", "Web browser")
    ui.show("main")
end

-- main entry
function main()
end

XUI()

-- Call main entry, keep it under
main()