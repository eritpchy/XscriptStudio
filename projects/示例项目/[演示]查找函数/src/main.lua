--------------------------------------------------
-- 脚本名称: 查找函数
-- 脚本版本: 2.0
-- 脚本作者: Jason
-- 脚本描述: 演示查找集合函数(find)的基本功能
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

-- 脚本入口
function main()
    print("现在开始测试find集合函数的代码")
    sleep(1500)
    -- 设置兼容模式手动截图
    --autoscreencap(true)
    --手动截图
    --screencap()
    --cap_and_find_path="/sdcard/Android/XScript/Dev/find_test.png"
    --screencap(cap_and_find_path,0,0,100,200)
    autoscreencap(false)
    sleep(5000)
    touch.click(100, 100)
    touch.down(0, 0)

    while true do
        screencap()
        sleep(200)
        timer = socket.gettime()

        flag, x, y = find.color(0x0000FF, 95)
        print(string.format("find.color全屏找蓝色 模糊度95 :x=%s ,y=%s,共耗时:%dms", x, y, (socket.gettime() - timer) * 1000), flag)
        touch.down(0, 0)
        sleep(100)
        touch.up(x, y)
    end

    timer = socket.gettime()
    flag, x, y = find.color(0x6EAB19, 90)
    print(string.format("find.color全屏找色 模糊度90:x=%s ,y=%s,共耗时:%dms", x, y, (socket.gettime() - timer) * 1000))
    touch.down(x, y)
    sleep(2000)
    touch.up(x, y)


    timer = socket.gettime()
    flag, x, y = find.color(0x6EAB19, 90, 1, 100, 500, 500)
    print(string.format("find.color区域找色 :x=%s ,y=%s,共耗时:%dms", x, y, (socket.gettime() - timer) * 1000))
    touch.down(x, y)
    sleep(2000)
    touch.up(x, y)
    sleep(2000)
    if flag then
        touch.click(x, y)
        sleep(2000)
        -- autoscreencap(false)
        timer = socket.gettime()
        toast(string.format("开始测试小范围16像素找色 x=%s y=%s", x, y))
        flag, x, y = find.color(0x6EAB19, 80, x + 1, y + 1, x + 5, y + 5)
        print(string.format("find.color小范围4像素找色 x=%s y=%s,共耗时:%dms", x, y, (socket.gettime() - timer) * 1000))
        touch.down(x, y)
        sleep(2000)
        touch.up(x, y)
    end

    xscript.stop()
end

-- 调用入口函数, 此行无论如何请保持最后一行
main()