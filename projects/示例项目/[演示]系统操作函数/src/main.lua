--------------------------------------------------
-- 脚本名称: 系统操作函数
-- 脚本版本: 2.0
-- 脚本作者: Jason
-- 脚本描述: 演示基本系统功能操作
--------------------------------------------------

function print(...)
    toast(json.encode{...})
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

-- 脚本入口
function main()
    print("最高亮度")
    system.setBrightness(100)
    sleep(3000)
    print("最低亮度")
    system.setBrightness(0)
    sleep(3000)
    print("自动亮度")
    system.setBrightness(-1)
    sleep(3000)
    print("现在准备关闭WIFI");
    system.setWifi(false)
    sleep(3000)
    print("现在准备打开WIFI");
    system.setWifi(true)
    sleep(1000)
    print("设置黏贴板");
    system.setClip("1234567890")
    sleep(1000)
    print("读取黏贴板: [" .. system.getClip() .. "]")
    sleep(1000)
    print("测试结束")
    sleep(2000)
end

-- 调用入口函数, 此行无论如何请保持最后一行
main()
