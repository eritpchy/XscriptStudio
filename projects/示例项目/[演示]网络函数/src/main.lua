--------------------------------------------------
-- 脚本名称: 网络函数
-- 脚本版本: 2.0
-- 脚本作者: Jason
-- 脚本描述: 基本网络操作功能演示
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

-- 等待回调的函数
function httpCallBack(data, code)
    -- 注意: 网络超时,或获取失败,异步的传入第一二参数(data,code)会是 nil,注意判断.
    print("回调http内容：", code, data)
end

-- 脚本入口
function main()
    print(net.get("http://www.qicaispace.com/teach/asp/page04/info45.asp"))


    --异步请求网络结果,默认超时30秒
    net.asyncGet("http://1212.ip138.com/ic.asp", httpCallBack, 10)

    sleep(3000)


    print("网络时间:[" .. os.date("%c", net.time()) .. "]")
    sleep(2000)

    print("准备下载百度首页  ")
    local res, code = net.get("http://www.baidu.com")
    if res ~= nil then
        print("http2返回码:" .. code .. "\n网页内容:" .. res)
        sleep(2000)

        content = net.stripHtml(res)
        print("返回去除HTML后的网页文本:", content)
        sleep(2000)
    end

    print("执行结束")
    sleep(2000)
end

-- 调用入口函数, 此行无论如何请保持最后一行
main()