--------------------------------------------------
-- 脚本名称: 界面演示 综合
-- 脚本版本: 2.0
-- 脚本作者: Jason
-- 脚本描述: 演示各种基本界面控件
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

print("md5:" .. security.md5("aaaaa"))
print("base64:" .. security.base64enc("aaaaa"))
print("base64dec:" .. security.base64dec(security.base64enc("aaaaa")))

--- 回调函数
function scp()
    scriptDir = xscript.scriptDir()
    pic_path = scriptDir .. ".screencap_test.png"
    screencap(pic_path, 0, 0, 100, 200)
    --绘制图片
    ui.addImageView("iv", pic_path)
    ui.setGravity("row5", 17)
    --图片设置点击回调
    ui.setOnClick("iv", "print(\"test:setOnClick:ImageView\")")
end

--- 回调函数
function onClick()
    print("按钮点击")
    ui.updateResult()
    result = ui.getResult("et_main")
    isChecked = ui.getResult("checkbox_main")
    selected = ui.getResult("rg")
    spResult = ui.getResult('spinner')

    ui.newRow("row6")
    ui.addTextView("tv_third", string.format("获取文字: %s 类型:%s", result, type(result)))

    ui.newRow("row7")
    ui.addTextView("tv_4", string.format("复选框 选择: %s  类型:%s", isChecked, type(isChecked)))

    ui.newRow("row8")
    ui.addTextView("tv_5", string.format("多选框 选择: %s 类型:%s", selected, type(selected)))

    ui.newRow("row9")
    ui.addTextView("tv_6", string.format("下拉框 选择: %s 类型:%s", spResult, type(spResult)))
    print("回调函数改成setText")
    ui.setOnClick("btn", "btn_onclick()")
    --ui.setEnabled("spinner",false)
    --动态重设UI内容
    ui.setTextView("tv_main", "Text +1!", 12, ui.WRAP_CONTENT, ui.WRAP_CONTENT)
    ui.setEditText("et_main", "hint  +1", ui.WRAP_CONTENT, ui.WRAP_CONTENT)
    ui.setRadioGroup("rg", { '4', '5', '6' }, 2)
    ui.setSpinner("spinner", { "1st  +1", "2nd  +1 ", "我是第3   +1", "4   +1", "五   +1" }, 2, -1, -1)
    ui.setCheckBox("checkbox_main", "checkbox  +1", true)
    ui.setButton("btn", "btn text  +1")
    ui.setImageView("iv", xscript.scriptDir() .. ".dama.png")
end

ii = 0
function btn_onclick()
    ii = ii + 1
    ui.setText("btn", ii)
end

--获取脚本运行路径

---------------------------------------------------------------------------------
-- 新建布局
ui.newLayout("maindialog")

---------------------------------------------------------------------------------
ui.newRow("row0")
--绘制文字
ui.addTextView("tv_main", "您好", 12, ui.MATCH_PARENT, ui.WRAP_CONTENT)

--文字设置背景颜色
--ui.setBackground("tv_main",0xFF006699)

--设置文字颜色
--ui.setTextColor("tv_main",0xFFFFFFFF)
--设置文字的边距
ui.setPadding("tv_main", 10, 5, 10, 5)
--设置文字居中
ui.setGravity("tv_main", ui.CENTER)
---------------------------------------------------------------------------------
-- 绘制新行
ui.newRow("row1")
ui.addEditText("et_main", "hint", ui.WRAP_CONTENT, ui.WRAP_CONTENT)

ui.addEditText("et_main1", "提示", ui.MATCH_PARENT, ui.WRAP_CONTENT)
ui.setText("et_main", "hello")
ui.setOnClick("et_main", "print(\"输入框被点击\",200)")
---------------------------------------------------------------------------------
-- 绘制新行
ui.newRow("row2")
--绘制单选组合项
ui.addRadioGroup("rg", { '1\n', '2', '3\n', '4', '5', '6' }, 2)
--单选组合项设置点击回调
ui.setTextColor("rg", 0xFF3366, 1)
ui.setOnClick("rg", "print(\"单选控件被点击\",200)")
---------------------------------------------------------------------------------
-- 绘制新行
ui.newRow("row3")
--绘制下拉列表项
ui.addSpinner("spinner", { "1st item", "2nd item", "我是第3", "4", "五" }, 2, ui.WRAP_CONTENT, ui.MATCH_PARENT)
ui.newRow("row4")
ui.addLine("")
--绘制新行
--ui.setPadding("spinner",30,0,0,0)
--ui.setPadding("row3",30,0,0,0)
ui.setTextColor("spinner", 0x33ff33, 0)
ui.setTextColor("spinner", 0xFF3366, 2)
ui.newRow("row4")
--下拉列表项设置点击回调
ui.setOnClick("spinner", "print(\"下拉控件被点击\",200)")
---------------------------------------------------------------------------------
-- 绘制新行
ui.newRow('2')
--绘制复选项
ui.addCheckBox("checkbox_main", "checkbox text", false)
--绘制新行
ui.newRow('1')
ui.setOnClick("checkbox_main", "print(\"复选控件被点击\",200)")
---------------------------------------------------------------------------------
-- 绘制按钮
ui.addButton("btn", "btn text")
--绘制新行
ui.setGravity("btn", 17)
--绘制按钮2
ui.addButton("btn2", "点击消失")
function gone()
    ui.setVisibility("btn2", ui.GONE)
end

ui.setOnClick("btn2", "gone()")
--绘制按钮3
ui.addButton("btn3", "点击隐藏")
ui.setOnClick("btn3", "	ui.setVisibility(\"btn3\",ui.INVISIBLE)")
ui.addButton("btn4", "4")

--UI多重回调
ii = "0"
function btn5call(ii)
    ii = tonumber(ii)
    ii = ii + 1
    ii = tostring(ii)

    ui.newLayout("btn5_ui" .. ii)
    ui.addButton("btn5" .. ii, "多重回调" .. ii)
    ui.setOnClick("btn5" .. ii, " btn5call(" .. ii .. ")")
    ui.addCheckBox("checkbox_main_sub", "复选框测试", false)
    ui.setCheckBox("checkbox_main_sub", "复选框", true)

    ii = tostring(ii)
    ui.show("btn5_ui" .. ii)
end

--绘制多重回调按钮
ui.addButton("btn5", "多重回调", 100)
--ui.setBackground("btn5",0xFFFF0000)
ui.setOnClick("btn5", "btn5call(" .. ii .. ")")

ui.newRow("row5")

---------------------------------------------------------------------------------


--加载上次的配置文件
ui.loadProfile()
--ui内容刷新
ui.updateResult()
--按钮设置点击回调
ui.setOnClick("btn", "print(\"enable onClick() start\")  onClick() ")
--ui.setOnClick("btn", "ui.dismiss('maindialog') ")


ui.setTitleText("maindialog", "综合界面演示")
ui.setTitleBackground("maindialog", 0xFFFF0000)
--ui.setBackground("maindialog",0xFF92b85c)
ui.setFullScreen("maindialog") --强制UI全屏显示,这个一定要 跟着ui.create后
--ui.setBackground("maindialog",0x33ff33)
ui.setOnClick("tv_main", "print(\"文本被点击\",200)")
setTrigger.ontime(50, scp)
ui.show("maindialog")


--保存配置文件
ui.saveProfile()
--	button.setPadding(left, top, right, bottom);


