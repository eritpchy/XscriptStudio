--------------------------------------------------
-- 脚本名称: 界面演示 标签
-- 脚本版本: 2.0
-- 脚本作者: Jason
-- 脚本描述: 在界面上添加多个标签
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

function XUI()
    -- 第一页数据
    ui.newLayout("1")
    ui.addTextView("tv1_1", "这是第一页")
    ui.newRow("row1")
    ui.addTextView("tv1_2", "这是第一页")
    ui.newRow("row2")
    ui.addTextView("tv1_3", "这是第一页")

    -- 第二页数据
    ui.newLayout("2")
    ui.addTextView("tv2_1", "这是第二页")
    ui.newRow("row3")
    ui.addEditText("et2_1", "提示", ui.MATCH_PARENT, ui.WRAP_CONTENT)

    -- 第三页数据
    ui.newLayout("3")
    ui.addTextView("tv3_1", "这是第三页")
    ui.newRow("row4")
    ui.addRadioGroup("rg3_1", { '1\n', '2', '3\n', '4', '5', '6' }, 2)

    -- 主页面
    ui.newLayout("main")
    -- 创建tab容器, 名称:tab
    ui.addTabView("tab")
    -- 将第一页放入tab中
    ui.addTab("第一页", "tab", "1")
    -- 将第二页放入tab中
    ui.addTab("第二页", "tab", "2")
    -- 将第三页放入tab中
    ui.addTab("第三页", "tab", "3")
    -- 设置标题
    ui.setTitleText("main", "界面演示 标签")
    -- 显示主页面
    ui.show("main")
end

-- 脚本入口
function main()
end

XUI()

-- 调用入口函数, 此行无论如何请保持最后一行
main()