--------------------------------------------------
-- 脚本名称: 快速通知示例
-- 脚本版本: 2.0
-- 脚本作者: Jason
-- 脚本描述: 快速更新通知演示
--------------------------------------------------

function print(...)
	toast(json.encode{...}, 1)
	logcatWithLevel(2, ...)
	logfileWithLevel(2, ...)
end

-- 脚本入口
function main()
	for i=1,1000000 do
		print(">>> " .. i .. " <<<")
	end
end

-- 调用入口函数, 此行无论如何请保持最后一行
main()