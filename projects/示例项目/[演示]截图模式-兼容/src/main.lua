--------------------------------------------------
-- 脚本名称: 截图模式 兼容
-- 脚本版本: 2.0
-- 脚本作者: Jason
-- 脚本描述: 兼容模式截图测试
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

function reCapture()
    rotc = rotc + 1
    if rotc > #rot then rotc = 1 end
    xscript.setProfile("rotation_fix", rot[rotc])
    screencap(pic_path)
    ui.setImageView("iv", pic_path)
end

function scp()
    local time, timediff
    time = os.clock()
    screencap()
    timediff = os.clock() - time

    -- 截屏到文件,这个函数支持bmp/png 格式
    screencap(pic_path)

    ui.newRow("")
    ui.setTextColor("tv_maina", 0xFFFF0000)
    ui.addTextView("tv_mainb", string.format("   截图耗时: %dms", timediff * 1000), 12)
    ui.newRow("")
    ui.addTextView("tv_mainc", "   截图文件保存在:" .. pic_path, 12)
    ui.newRow("")
    ui.addTextView("tv_maind", "    如显示方向与当前方向不符,请点击按钮手动修正:", 12)
    ui.setTextColor("tv_maind", 0xFFFF0000)
    ui.newRow("")
    ui.addButton("xiuzheng", "右转GO →")
    ui.setOnClick("xiuzheng", "reCapture()")
    ui.addTextView("tv_maine", "    (方向与屏幕不一致请手动修正)", 10)
    ui.setTextColor("tv_maine", 0xFFFF0000)
    ui.newRow("")
    -- 绘制图片
    ui.addImageView("iv", pic_path)
end

--
---------------------------------------------------------------------------------
-- 新建布局
function XUI()
    ui.newLayout("maindialog")
    ui.addTextView("tv_maina", "  看到截图就能使用兼容模式", 12)
    setTrigger.onUiShow("maindialog", "scp()")
    ui.setTitleText("maindialog", "截图模式 兼容")
    ui.show("maindialog")
end

-- 脚本入口
function main()
    -- 强制设置兼容模式
    toast("脚本启动")
    xscript.setCompatible(true)

    -- 获取脚本运行路径
    scriptDir = xscript.scriptDir()

    pic_path = scriptDir .. "/.screencap_co.png"
    --pic_path = scriptDir .. "/.screencap_co.bmp"

    rot = { 0, 90, 180, 270 }
    rotc = 1;
    XUI()
end

-- 调用入口函数, 此行无论如何请保持最后一行
main()