--------------------------------------------------
-- 脚本名称: 界面演示 浏览器
-- 脚本版本: 2.0
-- 脚本作者: Jason
-- 脚本描述: 一个小型浏览器
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

function GOTO()
    ui.updateResult()
    local url = ui.getResult("et")
    if (url ~= nil and type(url) == "string") then
        print(url)
        ui.setWebView("web", "http://" .. url)
        ui.refresh()
    end
end

function XUI()
    ui.newLayout("main")
    ui.addButton("btn", "转到")
    ui.addEditText("et", "baidu.com", ui.MATCH_PARENT, ui.WRAP_CONTENT)

    ui.setOnClick("btn", "GOTO()")
    ui.newRow("")
    ui.addWebView("web", "http://baidu.com")
    -- 设置标题
    ui.setTitleText("main", "界面演示 浏览器")
    ui.show("main")
end

-- 脚本入口
function main()
end

XUI()

-- 调用入口函数, 此行无论如何请保持最后一行
main()