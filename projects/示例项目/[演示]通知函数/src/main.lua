--------------------------------------------------
-- 脚本名称: 通知函数
-- 脚本版本: 2.0
-- 脚本作者: Jason
-- 脚本描述: 演示通知集合函数(notify)的基本功能
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

-- 脚本入口
function main()
    print("现在开始测试notify通知集合函数的代码")
    sleep(1500)

    print("测试notify.vibrate振动1秒")
    sleep(1500)
    notify.vibrate(1000)

    print("测试vibrate振动5秒")
    sleep(1500)
    notify.vibrate(5000)

    print("测试notify.toast")
    sleep(1500)
    notify.toast("Hello World", 10000)
    sleep(10000)

    print("测试notify.player播放媒体声音2秒")
    sleep(1500)
    notify.player("/system/media/audio/ringtones/Rush.ogg")
    sleep(2000)
    -- 停止播放
    notify.player("STOP")

    print("测试结束")
    sleep(1500)
end

-- 调用入口函数, 此行无论如何请保持最后一行
main()