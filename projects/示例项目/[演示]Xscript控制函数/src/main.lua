--------------------------------------------------
-- 脚本名称: Xscript控制函数
-- 脚本版本: 2.0
-- 脚本作者: Jason
-- 脚本描述: 演示如何用函数操作Xscript基本函数
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

-- 脚本入口
function main()
    --获取脚本运行路径
    scriptDir = xscript.scriptDir()

    print("开始演示xscript集合函数代码")
    sleep(2000)

    print(string.format("当前脚本运行路径:%s", xscript.scriptPath()))
    sleep(2000)
    print(string.format("当前脚本所在文件夹路径:%s", scriptDir))
    sleep(2000)

    print(string.format("当前程序是否为兼容模式:%s", xscript.isCompatible()))
    sleep(2000)
    xscript.setCompatible(true)
    print(string.format("现在修改兼容模式为true\n当前程序是否为兼容模式:%s", xscript.isCompatible()))
    sleep(2000)
    xscript.setCompatible(false)
    print(string.format("现在修改兼容模式为false\n当前程序是否为兼容模式:%s", xscript.isCompatible()))
    sleep(2000)
    print("设置配置文件:isCompatible=1")
    sleep(2000)
    xscript.setProfile("isCompatible", "1")
    print("脚本暂停")
    sleep(2000)
    xscript.pause()
    print("脚本恢复")
    sleep(2000)
    print("脚本停止!")
    sleep(2000)
    xscript.stop()
    print("脚本已停止,不应该看到这个")
    sleep(2000)
end

-- 调用入口函数, 此行无论如何请保持最后一行
main()