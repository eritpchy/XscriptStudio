--------------------------------------------------
-- 脚本名称: 按键函数
-- 脚本版本: 2.0
-- 脚本作者: Jason
-- 脚本描述: 演示按键集合函数(key)的基本功能
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

-- 脚本入口
function main()
    print("开始测试Key集合函数")
    sleep(2000)
    for ii = 97, 97 + 25 do
        print("测试按" .. string.char(ii), 490)
        sleep(500)
        key.click(string.char(ii))
    end

    print("按LEFT键")
    sleep(2000)
    key.click("DPAD_LEFT")

    print("按del键")
    sleep(2000)
    key.del()

    print("按退格键")
    sleep(2000)
    key.backSpace()

    print("按空格键")
    sleep(2000)
    key.space()

    print("按回车")
    sleep(2000)
    key.enter()
    --[[
    print("撤销")
    sleep(2000)
    key.undo()
    --]]
    print("按TAB键")
    sleep(2000)
    key.tab()

    print("按HOME")
    sleep(1000)
    key.click("HOME")
    print("按BACK")
    sleep(1000)
    key.click("BACK")
    print("按MENU")
    sleep(1000)
    key.click("MENU")
    print("长按HOME")
    sleep(1000)
    key.click("MENU", 1000)
    print("长按POWER")
    sleep(1000)
    key.down("POWER")
    sleep(2000)
    key.up("POWER")
    sleep(2000)
    key.power()
    sleep(2000)
    print("按音量- ")
    key.volDown()
    sleep(2000)
    print("按音量+ ")
    key.volUp()
    sleep(2000)

    print("按HOME 2")
    sleep(2000)
    key.home()

    print("按CALL 2")
    sleep(2000)
    key.call()

    print("按挂断通话键")
    sleep(2000)
    key.endCall()

    print("按TAB键")
    sleep(2000)
    key.tab()

    print("按BACK 2")
    sleep(2000)
    key.back()

    print("按menu 2")
    sleep(2000)
    key.menu()

    print("按power 2")
    sleep(2500)
    key.power()
    sleep(2000)

    key.power()

    print("按power 2")
    sleep(2000)
    print("测试结束")
    sleep(2000)
end

-- 调用入口函数, 此行无论如何请保持最后一行
main()