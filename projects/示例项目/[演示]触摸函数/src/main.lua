--------------------------------------------------
-- 脚本名称: 触摸函数
-- 脚本版本: 2.0
-- 脚本作者: Jason
-- 脚本描述: 演示触摸操作集合函数(touch)的基本功能
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

-- 脚本入口
function main()
    x, y = touch.scan("请点击一次屏幕")
    print("当前点击坐标:", x, y)


    touch.swipe(0, 0, 500, 600, 2000)
    sleep(1000)
    touch.down(200, 300, 1)
    touch.down(100, 100, 2)
    touch.down(500, 100, 3)
    touch.down(500, 100, 4)
    touch.down(500, 100, 5)
    touch.down(500, 100, 6)
    touch.down(500, 100, 7)
    touch.down(500, 100, 8)
    touch.down(500, 100, 9)
    touch.down(500, 100, 10)

    for iiii = 30, 130 do
        touch.move(iiii * 3, iiii * 2.5, 2)
        touch.move(iiii * 1.5, iiii * 1.5, 3)
        touch.move(iiii * 2.5, iiii * 1.5, 4)
        touch.move(iiii * 1.5, iiii * 3.5, 5)
        touch.move(iiii * 4.5, iiii * 1.5, 6)
        touch.move(iiii * 1.5, iiii * 5.5, 7)
        touch.move(iiii * 6.5, iiii * 1.5, 8)
        touch.move(iiii * 1.5, iiii * 7.5, 9)
        touch.move(iiii * 8.5, iiii * 1.5, 10)
        touch.move(iiii * 1.5, iiii * 9.5, 1)
        sleep(50)
    end
    touch.up(1)
    touch.up(2)
    touch.up(3)
    touch.up(4)
    touch.up(5)
    touch.up(6)
    touch.up(7)
    touch.up(8)
    touch.up(9)
    touch.up(10)

    print("移动坐标到 400x500")
    sleep(1500)
    touch.down(100, 200)
    sleep(1000)
    touch.move(400, 500)
    sleep(1000)
    touch.up()
    sleep(1500)

    sleep(1500)

    print("查找ICON并点击")
    sleep(1500)

    isfound, x, y = find.color(0x97ca4b, 96)
    if isfound then
        print(string.format("点击坐标%s,%s", x, y))
        sleep(1500)
        touch.click(x, y)
    else
        print("未找到!")
    end


    sleep(1500)
    print("测试结束")
end

-- 调用入口函数, 此行无论如何请保持最后一行
main()