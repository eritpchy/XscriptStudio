--------------------------------------------------
-- 脚本名称: 界面演示 官方网站
-- 脚本版本: 2.0
-- 脚本作者: Jason
-- 脚本描述: 打开Xscript官方网站
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

function XUI()
    ui.newLayout("main")
    ui.addWebView("web", "http://xdow.net")
    -- 设置标题
    ui.setTitleText("main", "官方网站")
    ui.show("main")
end

-- 脚本入口
function main()
end

XUI()

-- 调用入口函数, 此行无论如何请保持最后一行
main()