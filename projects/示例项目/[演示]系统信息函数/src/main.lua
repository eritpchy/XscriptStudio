--------------------------------------------------
-- 脚本名称: 系统信息函数
-- 脚本版本: 2.0
-- 脚本作者: Jason
-- 脚本描述: 演示统信息集合函数(systeminfo)的基本功能
--------------------------------------------------

--重写个print 以便输出结果到UI布局中.
count = 0
function print(...)
    local arg = { ... }
    for i, v in ipairs(arg) do
        v = tostring(v)
        count = count + 1
        ui.addTextView("main_ui_textview_N" .. count, v, 16, ui.MATCH_PARENT, ui.WRAP_CONTENT)
    end
    ui.newRow("main_ui_row_N" .. count)
end

--这是测试systeminfo集合的函数
function start()
    print("开始测试systeminfo集合函数")
    w, h = systeminfo.deviceSize()
    print(string.format("设备实际分辨率w=%s h=%s", w, h))
    w, h = systeminfo.displaySize()
    print(string.format("设备除虚拟按键分辨率w=%s h=%s", w, h))
    dpi = systeminfo.displayDpi()
    print(string.format("屏幕DPI为:%d", dpi))
    --获取网络 打开/关闭状态
    if systeminfo.networkstate() then
        print("已连接到互联网")
    else
        print("未连接到互联网")
    end
    --获取wifi 打开/关闭状态
    if systeminfo.wifistate() then
        print("当前wifi是打开状态")
    else
        print("当前wifi是关闭状态")
    end

    --这是测试getstatusbarheight函数的地方
    print("当前手机状态栏高度为" .. systeminfo.statusBarHeight())
    print("当前屏幕角度: " .. systeminfo.rotation())
    print("原始屏幕方向: " .. systeminfo.orientation())
    print("SD卡路径: " .. systeminfo.sdpath())
    print(string.format("蓝牙 MAC 地址:%s", systeminfo.bluetoothmac()))
    print(string.format("WIFI MAC 地址:%s", systeminfo.wifimac()))
    print(string.format("UDID:%s", systeminfo.udid()))
    print(string.format("IMEI:%s", systeminfo.imei()))
    print(string.format("型号:%s", systeminfo.model()))
    print(string.format("sdk版本号:%s", systeminfo.sdkversion()))
    print(string.format("发行版本号:%s", systeminfo.releaseversion()))
    print(string.format("主板代号:%s", systeminfo.board()))
    print(string.format("BOOTLOADER版本号:%s", systeminfo.bootloader()))
    print(string.format("BRAND号:%s", systeminfo.brand()))
    print(string.format("CPU_ABI:%s", systeminfo.cpuabi()))
    print(string.format("CPU_ABI2:%s", systeminfo.cpuabi2()))
    print(string.format("DISPLAY用户序列号:%s", systeminfo.display()))
    print(string.format("编译版本号:%s", systeminfo.fingerprint()))
    print(string.format("硬件名:%s", systeminfo.hardware()))
    print(string.format("HOST:%s", systeminfo.host()))
    print(string.format("ID修订号:%s", systeminfo.id()))
    print(string.format("制造商代号:%s", systeminfo.manufacturer()))
    print(string.format("全称:%s", systeminfo.product()))
    print(string.format("基带固件版本号:%s", systeminfo.radio()))
    print(string.format("硬件序列号:%s", systeminfo.serial()))
    --参考系统getprop命令
    print(string.format("读取系统prop值dhcp.wlan0.ipaddress: %s", systeminfo.getProp("dhcp.wlan0.ipaddress")))
    ui.refresh()
    sleep(1500)
end

function XUI()
    ------------- UI START---------------
    -- 使用无按钮的方式创建布局
    ui.newLayoutEmpty("main_ui")

    ui.addTextView("main_ui_textview_1", "\n脚本名称：systeminfo集合函数\n", 18, ui.matchParent, ui.wrapContent)
    ui.newRow("main_ui_row_1")
    ui.addButton("main_ui_btn", "点击启动")
    ui.setOnClick("main_ui_btn", "start() ")
    ui.newRow("main_ui_row_2")


    ui.setBackground("main_ui", 0xFFCCD8E9)

    ui.show("main_ui")
    ------------- UI END-----------------
end

XUI()
