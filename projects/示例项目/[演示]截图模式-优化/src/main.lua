--------------------------------------------------
-- 脚本名称: 截图模式 优化
-- 脚本版本: 2.0
-- 脚本作者: Jason
-- 脚本描述: 优化模式截图测试
--------------------------------------------------

local timediff

function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

function reCapture()
    rotc = rotc + 1
    if rotc > #rot then rotc = 1 end
    xscript.setProfile("rotation_fix", rot[rotc])
    screencap(pic_path)
    ui.setImageView("iv", pic_path)
end

function scp()

    time = socket.gettime()
    --截屏到文件,这个函数支持bmp/png 格式
    screencap(pic_path)
    timediff = socket.gettime() - time
    ui.newRow("Row1")
    ui.setTextColor("tv_maina", 0xFFFF0000)
    --由于screencap是兼容模式截图,故不能统计截图耗时
    --ui.addTextView("tv_mainb",string.format("   截图耗时: %dms",timediff2*1000), 15,  ui.MATCH_PARENT, ui.WRAP_CONTENT)
    --ui.newRow("Row2")
    ui.addTextView("tv_mainb", string.format("   截图和保存文件耗时: %dms", timediff * 1000), 15, ui.MATCH_PARENT, ui.WRAP_CONTENT)
    ui.newRow("Row3")
    ui.addTextView("tv_mainc", "  截图文件保存在:" .. pic_path, 15, ui.MATCH_PARENT, ui.WRAP_CONTENT)
    ui.newRow("Row4")
    ui.addTextView("tv_maind", "   如显示方向与当前方向不符,请点击按钮手动修正:", 15, ui.WRAP_CONTENT, ui.WRAP_CONTENT)
    ui.setTextColor("tv_maind", 0xFFFF0000)
    ui.newRow("Row5")
    ui.addTextView("tv_maine", "    [注意方向,请勿乱点！！！]", 15, ui.WRAP_CONTENT, ui.MATCH_PARENT)
    ui.setTextColor("tv_maine", 0xFFFF0000)

    ui.addButton("xiuzheng", "右转GO →")
    ui.setOnClick("xiuzheng", "reCapture()")
    ui.newRow("Row6")
    --绘制图片
    ui.addImageView("iv", pic_path)
end

--
---------------------------------------------------------------------------------
-- 新建布局
function XUI()
    ui.newLayout("maindialog")
    ui.addTextView("tv_main", "  优化模式截图测试", 15, ui.MATCH_PARENT, ui.WRAP_CONTENT)
    ui.newRow("Row0")
    ui.addTextView("tv_maina", "  看到截图就能使用优化模式", 15, ui.MATCH_PARENT, ui.WRAP_CONTENT)
    setTrigger.onUiShow("maindialog", "scp()")
    ui.setTitleText("maindialog", "截图模式 优化")
    ui.show("maindialog")
end


-- 脚本入口
function main()
    --强制设置优化模式
    xscript.setCompatible(false)

    --获取脚本运行路径
    scriptDir = xscript.scriptDir()

    --pic_path = scriptDir .. "/.screencap_op.png"
    pic_path = scriptDir .. "/.screencap_op.bmp"

    rot = { 0, 90, 180, 270 }
    rotc = 1;
    XUI()
end

-- 调用入口函数, 此行无论如何请保持最后一行
main()