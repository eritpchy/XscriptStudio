--------------------------------------------------
-- 脚本名称: 查看输入法包名
-- 脚本版本: 2.0
-- 脚本作者: Jason
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

function getIme()
    local list = {}
    local str
    local imeTmp = xscript.scriptDir() .. ".ime"
    --注意: 请勿使用io.popen, 容易受安卓权限影响
    os.execute("ime list -s > " .. imeTmp .. ";exit")
    str = xutil.readFileAll(imeTmp)
    if (str ~= nil) then
        list = xutil.split(str, "\n")
    end
    return list
end

function copy(id)
    system.setClip(id)
    print("已复制 [" .. id .. "]", 1000)
end

-- 脚本入口
function main()
    print("脚本已启动")
    local i
    local imes = getIme()
    ui.newLayout("main")
    ui.setTitleText("main", "输入法包名列表")
    logcat(#imes)
    ui.addTextView("label_copy", "点击复制")
    ui.newRow("")
    for i = 1, #imes do
        if imes[i] and imes[i]:len() > 0 then
            local imeID = "ime_" .. i
            ui.addTextView(imeID, imes[i], 14, ui.WRAP_CONTENT, 40)
            ui.setGravity(imeID, ui.BOTTOM)
            ui.setOnClick(imeID, "copy([[" .. imes[i] .. "]])")
            ui.newRow("row1_" .. i)
            ui.addLine("line_" .. i)
            ui.newRow("row2_" .. i)
        end
    end
    ui.setTitleText("main", "查看输入法包名")
    ui.show("main")
end

-- 调用入口函数, 此行无论如何请保持最后一行
main()
