--------------------------------------------------
-- 脚本名称: [演示]设置UI
-- 脚本描述: 演示基本的设置UI, 以及取值获取
--------------------------------------------------

function printValues()
    while true do
		sleep(2000)
		local uiCache = ui.getData()
        toast(string.format("控件1:%s\n控件2:%s\n控件3:%s\n控件4:%s\n控件5:%s\n控件6:%s", 
		uiCache["ed1"], uiCache["sb1"], uiCache["rg1"], uiCache["rg2"], uiCache["rg3"], uiCache["rg4"], uiCache["ed1"]), 2000)
    end
end

-- 脚本入口
function main()
	printValues()
end


-- 此行无论如何保持最后一行
main()
