--------------------------------------------------
-- 脚本名称: 多线程综合测试
-- 脚本描述: 
--------------------------------------------------
-- 这里是主线程
local gvar_1 = 1

require 'loopThread'
require 'watchThread'

-- 脚本入口
function main()
	-- 创建跨线程全局变量
	thData = thread.newGlobalVar("thData")
	-- 启动loopThread 线程
	thread.start(loopThread, 1, 10000,10000)
	-- watchThread 线程
	thread.start(watchThread, 2, 10000,10000)
	-- 保证主线程不退出
	while true do
		sleep(50000)
	end
end
-- 此行无论如何保持最后一行
main()
