--------------------------------------------------
-- 脚本名称: 界面演示 本地html
-- 脚本描述:
--------------------------------------------------

function print(...)
    toast(json.encode{...})
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

function XUI()
	ui.newLayout("main")
    ui.addWebView("web","file://" .. xscript.scriptDir() .. "/.local.html")
    ui.setEventHandler("web", function(id, type, ...)
        local args = {...}
        print("点击", id, type, unpack(args))
        print(args[1])
    end)
    ui.setTitleText("main", "[演示]界面-本地html")
    ui.show("main")
end

-- 脚本入口
function main()
end

XUI()

-- 调用入口函数, 此行无论如何请保持最后一行
main()
