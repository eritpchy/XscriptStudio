--------------------------------------------------
-- 脚本名称: Xml操作函数
-- 脚本描述: 演示基本xml数据、文件操作
--------------------------------------------------

--重写个print 以便输出结果到UI布局中.
function print(...)
    local arg = { ... }
    for i, v in ipairs(arg) do
        v = tostring(v)
        ui.addTextView("main_ui_textview_N", v, 16, ui.matchParent, ui.wrapContent)
        ui.addTextView("main_ui_textview_N", " ", 16, ui.matchParent, ui.wrapContent)
    end
    ui.newRow("main_ui_row_N")
end

function X_xml()
    ui.addTextView("main_ui_textview_2", "--------------", 16, ui.matchParent, ui.wrapContent)
    ui.newRow("main_ui_row_3")
    ui.addTextView("main_ui_textview_3", "执行结果:", 16, ui.matchParent, ui.wrapContent)
    ui.newRow("main_ui_row_4")
    ui.addTextView("main_ui_textview_4", "从.xml.xml文件中取得结果:", 16, ui.matchParent, ui.wrapContent)
    ui.newRow("main_ui_row_5")

    -- load XML data from file ".xml" into local table xfile
    local success, xfile = pcall(function() return xml.load(xscript.scriptDir() .. "/.xml.xml") end)
    if success == false or xfile == nil then success, xfile = pcall(function() return xml.load(xscript.scriptDir() .. "/xml.xml") end) end
    if success == false or xfile == nil then success, xfile = pcall(function() return xml.load("/sdcard/Android/XScript/示例脚本/.xml.xml") end) end
    if success == false or xfile == nil then success, xfile = pcall(function() return xml.load("/sdcard/Android/XScript/Demo/.xml.xml") end) end

    print(success, xfile)
    -- search for substatement having the tag "scene"
    local xscene = xfile:find("scene")
    -- if this substatement is found
    if xscene ~= nil then
        --  print it to screen
        print(xscene)
        --  print  tag, attribute id and first substatement

        print(xscene:tag())
        print(xscene.id)
        print(xscene[1])
    end
    --获取当前运行脚本所在文件夹
    local dir = xscript.scriptDir()
    --保存table成xml
    xfile:save(dir .. "/t.xml")
    print("---执行成功---.")
end


function XUI()
    ------------- UI START---------------
    ui.newLayout("main_ui")
    ui.addTextView("main_ui_textview_1", "\n脚本名称：XML支持测试脚本\n", 18, ui.matchParent, ui.wrapContent)
    ui.newRow("main_ui_row_1")
    ui.addButton("main_ui_btn", "点击启动")
    ui.setOnClick("main_ui_btn", "X_xml() ")
    ui.newRow("main_ui_row_2")

    -- 设置标题
    ui.setTitleText("main_ui", "Xml操作函数")
    ui.show("main_ui")
    ------------- UI END-----------------
end

-- 脚本入口
function main()
end

XUI()

-- 调用入口函数, 此行无论如何请保持最后一行
main()