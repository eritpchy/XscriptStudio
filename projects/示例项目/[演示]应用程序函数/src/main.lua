--------------------------------------------------
-- 脚本名称: 应用程序函数
-- 脚本版本: 2.0
-- 脚本作者: Jason
-- 脚本描述: 演示应用程序函数库(app)的基本功能
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

-- 脚本入口
function main()
    print("现在开始测试app集合函数的代码")
    sleep(1500)
    print(string.format("当前前台应用包名为:%s\n本应用包名为:%s", app.getfront(), app.getSelf()))
    sleep(1500)

    print("现在检测腾讯qq是否安装了")
    sleep(1500)
    isinstalled = app.isinstall("com.tencent.mobileqq")
    print("腾讯QQ已安装")
    sleep(1500)

    print("现在启动到腾讯qq")
    app.start("com.tencent.mobileqq")
    sleep(1500)

    print(string.format("当前前台应用包名为:%s", app.getfront()))
    sleep(1500)

    print("现在切换到上一个应用")
    sleep(1500)
    app.switch()
    sleep(1500)

    print("现在切换到腾讯qq")
    sleep(1500)
    app.switch("com.tencent.mobileqq")
    sleep(1500)

    print("现在测试结束腾讯qq")
    sleep(1500)
    app.stop("com.tencent.mobileqq")


    print("现在测试冻结腾讯QQ")
    sleep(1500)
    app.disable("com.tencent.mobileqq")
    print("冻结成功,请打开设置查看")
    sleep(5000)

    print("现在测试解冻腾讯QQ")
    sleep(1500)
    app.enable("com.tencent.mobileqq")
    print("解冻成功,请打开设置查看")
    sleep(5000)

    --print("现在测试清空腾讯QQ数据")
    --sleep(1500)
    --app.clear("com.tencent.mobileqq")
    --sleep(3000)

    print("现在测试安装新应用")
    sleep(1500)
    is_success = app.install("/sdcard/Download/test.apk")
    print("安装成功")


    print("现在测试卸载刚刚安装上的迅雷应用")
    sleep(1500)
    is_success = app.unInstall("com.xunlei.downloadprovider")
    print("卸载成功")

    print("现在测试杀掉所有后台应用")
    sleep(1500)
    app.killall()
    sleep(1500)

    print("现在测试列出所有应用")
    sleep(1500)
    app_list = app.list()
    ii = 1
    ui.newLayout("main")
    while (app_list[ii] ~= nil) do
        ui.addTextView("app_" .. ii, app_list[ii])
        ui.newRow("")
        ui.addLine("line_" .. ii)
        ui.newRow("")
        ii = ii + 1
    end
    ui.setTitleText("main", "应用程序函数")
    ui.show("main")
    print("测试结束")
    sleep(1500)
end

-- 调用入口函数, 此行无论如何请保持最后一行
main()