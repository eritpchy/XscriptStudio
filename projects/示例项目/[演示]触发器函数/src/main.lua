--------------------------------------------------
-- 脚本名称: 触发器函数
-- 脚本版本: 2.0
-- 脚本作者: Jason
-- 脚本描述: 演示基本异步操作函数
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

function ontimeloop()
    if gVar.ii > 10 then
        exit()
    end

    print("循环计时触发器触发,次数" .. gVar.ii .. "次,10次后测试exit函数强制退出,经过时间:" .. (socket.gettime() - gVar.timer) * 1000 .. "ms")
    gVar.ii = gVar.ii + 1
end

function ontime()
    print("触发器触发,经过时间:" .. (socket.gettime() - gVar.timer) * 1000 .. "ms")
end

-- 脚本入口
function main()
    print("开始测试setTrigger集合函数和exit函数...")
    sleep(1500)

    print("设置时间触发器,延迟3秒执行")
    gVar = thread.newGloablVar("gVar")
    gVar.timer = socket.gettime()
    gVar.ii = 1

    setTrigger.ontime(3000, ontime)
    setTrigger.timeloop(2000, ontimeloop)

    -- 阻止主线程退出
    for ii = 1, 100000 do
        sleep(200)
    end
end

-- 调用入口函数, 此行无论如何请保持最后一行
main()