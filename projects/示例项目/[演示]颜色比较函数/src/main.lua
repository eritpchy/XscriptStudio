--------------------------------------------------
-- 脚本名称: 颜色比较函数
-- 脚本版本: 2.0
-- 脚本作者: Jason
-- 脚本描述: 演示基本颜色比较函数
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

-- 脚本入口
function main()
    touch.click(100, 100)
    print(getcolor(100, 100))
    -- 设置兼容模式手动截图
    autoscreencap(false)
    --手动截图
    screencap()
    timer = os.clock()
    for ii = 1, 100 do

        iscolor(58, 44, 0x97ca4b, 99)
        --sleep(2500)
        --toast(getcolor(200,200))

        --sleep(2500)
        --x,y=findcolor(0xBFAA0D,90,1,1,500,100)
        --toast(string.format("区域找色:x=%s ,y=%s",x,y))
    end

    print("共耗时:" .. os.clock() - timer)
    sleep(2000)
end

-- 调用入口函数, 此行无论如何请保持最后一行
main()
