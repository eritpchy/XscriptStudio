--------------------------------------------------
-- 脚本名称: 控制条函数
-- 脚本版本: 1.1
-- 脚本作者: J
-- 脚本描述: 演示控制条集合函数(bar)的基本功能
--------------------------------------------------
-- 同步输出消息
function syncToast(msg)
    toast(msg, 2000)
    sleep(2000)
end

function main()
    syncToast("开始咯")
    sleep(1000)
    -- 记住控制条位置
    local x, y = bar.position()
    syncToast("控制条移动到200x200的位置")
    bar.position(200, 200)
    syncToast("控制条隐藏")
    bar.hide()
    syncToast("当前控制条状态 显示?: " .. tostring( bar.isShown()))
    syncToast("控制条恢复")
    bar.show()
    syncToast("当前控制条状态 显示?: " .. tostring( bar.isShown()))

    -- 恢复控制条位置
    bar.position(x, y)
    syncToast("测试结束")
    sleep(1000)
end

main()