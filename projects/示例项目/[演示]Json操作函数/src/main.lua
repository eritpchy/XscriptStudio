--------------------------------------------------
-- 脚本名称: Json操作函数
-- 脚本版本: 2.0
-- 脚本作者: Jason
-- 脚本描述: 演示基本json数据操作
--------------------------------------------------
function print(...)
    toast(json.encode { ... })
    logcatWithLevel(2, ...)
    logfileWithLevel(2, ...)
end

-- 脚本入口
function main()
    --- -带数组的复杂数据-----
    local _jsonArray = {}
    _jsonArray[1] = 8
    _jsonArray[2] = 9
    _jsonArray[3] = 11
    _jsonArray[4] = 14
    _jsonArray[5] = 25

    local _arrayFlagKey = {}
    _arrayFlagKey["array"] = _jsonArray

    local tab = {}
    tab["Himi"] = "Xscript"
    tab["testArray"] = _arrayFlagKey
    tab["age"] = "23"

    --数据转json
    local jsonData = json.encode(tab)

    print(jsonData)
    sleep(2000)
    -- 打印结果： {"age":"23","testArray":{"array":[8,9,11,14,25]},"Himi":"Xscript"}

    --json转数据
    local data = json.decode(jsonData)
    local a = data.age
    local b = data.testArray.array[2]
    local c = data.Himi

    print("a:" .. a .. "  b:" .. b .. "  c:" .. c)
    sleep(2000)
    -- 打印结果： a:23  b:9  c:Xscript
end

-- 调用入口函数, 此行无论如何请保持最后一行
main()